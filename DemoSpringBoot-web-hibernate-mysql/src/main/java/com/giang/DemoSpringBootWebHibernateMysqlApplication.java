package com.giang;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoSpringBootWebHibernateMysqlApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoSpringBootWebHibernateMysqlApplication.class, args);
	}
}
