/** 
 * create a Acount Object --> table "account"
 */
package com.giang.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Giang
 *
 */
@Entity
@Table(name = "account")
public class Account {

	@Id
	@Column(name = "username")
	private String username;

	@Column(name = "password")
	private String password;

	public Account() {
		// TODO Auto-generated constructor stub
	}

	public Account(String username, String password) {
		super();
		this.username = username;
		this.password = password;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "Account: " + this.username + ", " + this.password;
	}
}
