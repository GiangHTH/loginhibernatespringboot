package com.giang.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.*;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import com.giang.model.Account;
import com.giang.util.HibernateUtil;


@Controller
public class AccountController {
	
	// register page
	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public String Register() {
		return "register";
	}

	//check user - pass - re-pass to add value into account table
	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public String Register(HttpServletRequest request, ModelMap model) {
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		String repassword = request.getParameter("repassword");

		if (username == "" || password == "" || repassword == "") {
			model.addAttribute("error", "Mời đăng kí");
			return "register";
		} else {
			if (password.equals(repassword)) {
				Session session = HibernateUtil.getSessionFactory().openSession();
				//@SuppressWarnings("unchecked")
				Query query = session.createQuery("FROM Account WHERE username = :username");
				query.setParameter("username",username);
				List<Account> accs = query.list();
				if (accs.isEmpty()) {
					Account acc = new Account(username, password);
					session.beginTransaction();
					session.save(acc);
					session.getTransaction().commit();
					session.close();
					model.addAttribute("mess", "Hello " + username);
					return "home";
				} else {
					model.addAttribute("error", "Tên đăng nhập đã tồn tại");
					return "register";
				}
				
			} else {
				model.addAttribute("error", "Mật khẩu không trùng khớp");
				return "register";
			}
		}
	}
	
	// login page
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String Login() {
		return "login";
	}
	
	//check account exist or not
	//if exist, go to home page
	//if not, come back login page
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String Login(HttpServletRequest request, ModelMap model) {
		String username = request.getParameter("username");
		String password = request.getParameter("password");

		if (username == "" || password == "") {
			model.addAttribute("error", "Mời đăng kí");
			return "login";
		} else {
			Session session = HibernateUtil.getSessionFactory().openSession();
			Query query = session.createQuery("FROM Account WHERE username = :username and password = :password");
			query.setParameter("username",username);
			query.setParameter("password",password);
			List<Account> accs = query.list();
			session.close();
			
			if (!accs.isEmpty()) {
				model.addAttribute("mess", "Hello " + username);
				return "home";
			} else {
				model.addAttribute("error", "mật khẩu không tồn tại, mời nhập lại");
				return "login";
			}
		}
	}
}
